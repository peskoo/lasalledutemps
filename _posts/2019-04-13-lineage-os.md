---
layout: post
title: "Installer LineageOS sur un téléphone intelligent."
categories: [hello world, tutoriel]
comments: true
---
Le libre n'étant malheureusement pas encore la chose la plus accessible aux
utilisateurs les plus courants.   
Voici une version simplifiée, autant que faire ce peux, pour passer votre 
mobile sur Lineage OS.

<!--more-->

Je vais éviter d'entrer dans une diabolisation des GAFAM mais si chacun tente
de comprendre par où et comment ils récoltent des données dans notre quotidien,
on est déjà sur un bon chemin !

<iframe width="560" height="315" src="https://www.youtube.com/embed/hLjht9uJWgw?controls=0"
	frameborder="0" 
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
	allowfullscreen>
</iframe>

Je te conseil sur Android les applications:  
[Bouncer](https://play.google.com/store/apps/details?id=com.samruston.permission&hl=en_US),
[Exodus Privacy](https://play.google.com/store/apps/details?id=org.eu.exodus_privacy.exodusprivacy) et
[Firefox focus](https://play.google.com/store/apps/details?id=org.mozilla.focus).   
Ces trois applications aident beaucoup sur cette voie de reprise de controle de
votre vie privée.

### ADB (*Android Debug Bridge*)
**ATTENTION**, avant toute chose vérifies si ton téléphone est dans cette 
[liste](https://wiki.lineageos.org/devices/).

Entrons dans le vif du sujet, et commençons l'installation d'adb sur ta machine.  
Adb va nous servir pour communiquer avec notre petit téléphone directement de notre ordi.   

Linux:  
1. Télécharges ce [fichier .zip](https://dl.google.com/android/repository/platform-tools-latest-linux.zip)

MacOS:  
1. Télécharges ce [fichier .zip](https://dl.google.com/android/repository/platform-tools-latest-darwin.zip)

Windows: 
1. Changes d'OS.   
Apparemment il faut ajouter la [variable d'environnement](https://www.computerhope.com/issues/ch000549.htm):    
`;%USERPROFILE%\adb-fastboot\platform-tools`     
Puis installer ce [driver](https://github.com/koush/UniversalAdbDriver).    

 


**Sur Linux & Mac**:
- Crées un nouveau dossier là où la vie te semble la plus douce
- Dezippes ce .zip fraichement téléchargé dans ce nouveau dossier 
- Ouvres un terminal
- Et selon ce que tu as (bash, zsh, etc) tu copies les quelques lignes 
qui vont arrivées dans l'un de ces fichiers:  
  - .bashrc, .zshrc, .profile, etc. 
Si tu ne sais pas quel shell tu as sous la main, pas de panique.  
Toujours dans le terminal, tu fais un joli `echo $0` et la reponse tu trouveras.
- Copies ces lignes dans le dit fichier:
```bash
if [ -d "$HOME/chemin_vers_le_dossier/platform-tools" ] ; then
 export PATH="$HOME/chemin_vers_le_dossier/platform-tools:$PATH"
fi
```
- Et relance le tout.

Exemple de mon historique de commandes, je suis sous zsh ([oh-my-zsh](ihttps://ohmyz.sh/)):    
```bash
$ mkdir ~/adb
$ mv ~/downloads/platform-tools_r28.0.2-linux.zip ~/adb
$ unzip ~/adb/platform-tools_r28.0.2-linux.zip
$ vim ~/.zshrc
	if [ -d "$HOME/adb/platform-tools" ] ; then
 	 export PATH="$HOME/adb/platform-tools:$PATH"
	fi
$ source ~/.zshrc  
```

- Étape chiante supplémentaire pour ceux sous linux:   

Exemple ici sous ubuntu 18+, pour les autres distributions je t'invite par 
[ici](https://github.com/M0Rf30/android-udev-rules#installation).   

```bash
git clone https://github.com/M0Rf30/android-udev-rules.git
cd android-udev-rules
sudo cp -v 51-android.rules /etc/udev/rules.d/51-android.rules
sudo chmod a+r /etc/udev/rules.d/51-android.rules
sudo groupadd adbusers
sudo usermod -a -G adbusers $(whoami)
sudo udevadm control --reload-rules
sudo service udev restart
adb kill-server

# Connecte ton téléphone par USB à l'ordinateur 
adb devices

# Tu devrais voir par exemple
List of devices attached
* deamon not running; starting now at tcp:5037
* deamon started successfully
9ba6d834	device
```

### Lancer adb sur ton téléphone

1. Ça va être foufou, on va activer le mode développeur sur le smartphone.
- Sur ton 3310 tu vas dans **Paramètres**
-  **A propos du téléphone**
-  Tu tapotes 7 fois sur **Numéro du build** tout en bas.
-  Reviens en arrière
-  **Système** > **Options pour les développeurs**
- Active **Déverouillage OEM**
- Tu descends jusqu'à la section *Débogage*
- Active **Débogage Android**
2. Si tu as débranché ton téléphone, rebranche le.
3. Lance un terminal et tapouille:
- `adb devices`
- Une boite de dialogue devrait s'afficher sur le téléphone, tu valides.


Ok, commençons les choses sympathiques !   
Toujours dans le terminal:  
- `adb reboot bootloader`  
  Tu es maintenant en fastboot mode.  
  Avec les touches du volumes, tu peux changer d'option et valider avec le bouton
  power. Mais là tu n'as pas à le faire.
- `fastboot devices`  
  Tu devrais voir afficher un id, celui de ton téléphone.
- `fastboot oem unlock`  
  Il va faire sa tambouille, puis une fois redémarré, check dans les options si
  *Débogage Android* est toujours activé.

Le mode recovery de base n'est pas celui que nous utiliserons, on va prendre **TWRP**.  
- Télécharges [TWRP](https://twrp.me/Devices/) suivant le modèle de ton mobile.
- Connectes ton téléphone en USB si tu l'as débranché mais globalement il n'y a
  pas de mystère... Laisse le brancher.
- Dans le terminal:   
  ```bash
  $ adb reboot bootloader   
  $ fastboot flash recovery chemin/vers/twrp-x.x.x-x-x.img
  ```
- Et reboot ton télephone.   
  Tu dois pouvoir relancer une petite commande comme 
`adb reboot bootloader` 
- Maintenant tu peux utiliser les touches volumes pour séléctionner le 
**Recovery mode** et valider avec power.


### Sprint final: Lineage OS !

1. Choisis l'image que tu veux installer parmis cette 
[liste](https://download.lineageos.org/oneplus3)  
  Tu ne dezippes rien cette fois. Tu laisses le fichier se télécharger et tu reviens
sur ton téléphone.
2. Je ne peux que te conseiller de faire un backup. Mais ce n'est pas une étape
obligatoire.
3. Tapouille sur **Wipe**
  - *Format Data* et tape 'yes'
  - Reviens et fais *Advanced Wipe* cette fois. Coche *cache* et *system* > 
*Swipe to wipe*
4. **Advanced** puis **ADB Sideload**
5. Dans le terminal de votre ordinateur, tapes:  
  `adb sideload chemin/vers/lineage.zip`
6. S'il ne reboot pas tout seul, **Reboot** > **System**

#### Bimbadaboom, tu viens de passer sur Lineage OS !


## Ajouter un store
- [Aptoide](https://fr.aptoide.com/installer) (Applications disponibles sur le Play
Store sans utiliser le Play Store)
- [F-Droid](https://f-droid.org/fr/) (Applications libres)
- [OpenGApps](https://opengapps.org/) (Applications Google)

Si tu choisis OpenGApps, un conseil selectionnes la variant 'pico' qui 
contient seulement le Play Store.  
Ensuite pour l'installation tu utilises le **Recovery mode** avec **ADB Sideload**
exactement comme on vient de le faire avec l'image de lineage.

Si tu as une question, un doute, un problème:   
- [Subreddit LineageOS](https://www.reddit.com/r/LineageOS/)
- [Viens m'embêter sur Twitter](https://www.twitter.com/Pesko_)
