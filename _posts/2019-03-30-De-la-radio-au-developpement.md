---
layout: post
title: "De la radio au développement, le chemin d'une reconversion."
categories: [hello world]
comments: true
---
Qu'on vienne de la radio, de la tapisserie, du trading ou de la boulangerie.     
Qu'on ait 2, 5, ou 15 ans d'expériences.    
La reconversion fait qu'on redevient le petit nouveau, il faut l'accepter et à 
terme on n'est pas peu fier du chemin parcouru.

<!--more-->
Je vous conseille cet album pour rendre cette lecture agréable :                  
  
[![Melodysheep - The Arrow of Time](
https://f4.bcbits.com/img/a2443325110_16.jpg){:width="30%" .img-left}](
https://melodysheep.bandcamp.com/album/the-arrow-of-time-soundtrack-to-timelapse-of-the-future)

On est bon ? Alors c'est parti.         
*Permettez-moi de faire des petits sauts dans le temps*, ça va être rapide et 
ça sera en guise de présentation:

- 2004 - J'ai 16 ans, j'apprends le mixage/mastering en trifouillant 
[protools](https://fr.wikipedia.org/wiki/Pro_Tools & co") 
(Pas trop de tuto youtube à cette époque)

- 2008 - Après le deuxième échec au BAC (Les études n'étaient pas ma tasse de 
thé, alors qu'aujourd'hui j'adore le thé.), j'entre dans une école 
audiovisuelle (ESRA/ISTS)

- 2009 - Je vous passe les détails mais le fait que je n'ai pas le fameux bac 
m'empêche de continuer en fin de 1ère année.

- 2009 - J'entre en école de radio (STUDEC)

- 2013 - Producteur pour Chérie FM (Groupe NRJ)

- 2017 - Formation, Le Wagon Paris

- 2018 - Backend développeur pour Outscale (Dassault system)

*Spoiler alert*, il n'était pas du tout prévu que je sorte développeur de 
cette reconversion.      
Avant de quitter mon boulot en radio je m'intérressais aux languages web 
[html](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language), 
[css](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade), 
[javascript](https://fr.wikipedia.org/wiki/JavaScript) et deux, trois 
[cms](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu) comme 
[drupal](https://fr.wikipedia.org/wiki/Drupal), 
ou le bon vieux [wordpress](https://fr.wikipedia.org/wiki/WordPress); 
afin de pouvoir réaliser et mettre en ligne des projets que j'avais en tête. 
Donc ce que j'aimais faire, c'était de tester des projets (plus ou moins foireux). 

Puis arrive le [projet](https://peskoo.github.io/wetrip2017/) qui me motive 
plus que d'autres et ma première erreur.

### Décembre 2016 - Wetrip 

Une rupture conventionnelle dans le sac à dos, on y va... L'objectif, monter ce projet. 

Alors est-ce qu'avant de partir j'ai un 
[MVP](https://fr.wikipedia.org/wiki/Produit_minimum_viable) ? **Non**.    
Une idée de la stack technique nécessaire ? **Non**.        
Un marché ? **Non**.      

Ouais c'est nimp. Mais je ne m'en rends pas compte, je suis content. Surtout con.        
Ce sont des choses à faire avant de tout quitter, histoire d'avoir une idée de 
là où vous allez, un petit filet de sécurité.


Plusieurs mois passent... Je me forme à l'entreprenariat, je pivote afin de 
coller au marché que je cible et surtout pour avoir un business plan qui ne fasse pas 
trop peur niveau chiffre d'affaire. Je travail à rendre l'idée viable, 
[scalable](https://fr.wikipedia.org/wiki/Scalability) etc. 
Mais je ne m'intéresse toujours pas au côté technique. J'ai seulement une [landing 
page](https://en.wikipedia.org/wiki/Landing_page) et un beau logo. Haha.

Dans ma tête je comptais porter le projet, le présenter, entrer dans un 
[incubateur](https://fr.wikipedia.org/wiki/Incubateur_d%27entreprises)
et trouver un développeur sympathique qui serait tenter de me suivre puis 
réaliser tout le back, le front, l'infra etc. Un magicien sympa qui veut vivre
de lignes de code et d'eau fraiche. Attendez, un dev c'est avant tout un
passionné, okay ?

### Avril 2017 - Product Manager

Je suis sur la route pour Lille afin de présenter tout ça devant un jury de 
[via-ID](https://www.via-id.com/).      
Pour dire à quel point je suis un peu à la ramasse, je n'avais pas de 
présentation. Du coup, je me retrouve 30mn avant dans la voiture en train de 
bosser la présentation que m'avait faite ma copine à l'époque en ayant appris 
que j'y allais les mains dans les poches *(Quand ta scolarité te rattrape. 
J'étais à deux doigts de leurs demander une copie double et un stylo)*. 

La suite est logique, c'était de loin le meilleur pitch qu'ils aient vu de la
journée...      
Non, bien évidemment que non. Mais sur les phases de questions,
j'étais là, je connaissais mon marché et mon produit. 

Pas de surprise, je suis refusé. Cette expérience m'a fait prendre conscience
que je voulais peut-être aller trop vite. Généralement lorsqu'on porte un projet,
au-delà de l'idée, on vend une équipe où chacun amène une sécurité avec son
parcours pour rassurer ceux qui parient sur toi. Moi je suis seul et je viens de radio.

Ok, alors il faut que je me crée une crédibilité sur le CV. Est-ce qu'il y a un
métier qui peut m'apporter de l'expérience cohérente avec ce projet sans que ça
prenne mille ans ?       
Oui, **[product manager](https://en.wikipedia.org/wiki/Product_manager)**.

### Juin 2017 - Le Wagon

C'est reparti, qu'est-ce qu'il faut pour être un bon PM ?            
Je développe ma vision produit, je lis des bouquins sur 
l'[UX/UI](https://fr.wikipedia.org/wiki/Exp%C3%A9rience_utilisateur), je fais des 
[MOOC](https://fr.wikipedia.org/wiki/Massive_Open_Online_Course). 
Il faut avoir une vision technique aussi: j'apprends 
[Python](https://fr.wikipedia.org/wiki/Python_(langage)), j'approfondi mes 
bases en html, css et javascript. 
Mais lorsque je vais chercher un emploi, le problème restera le même: je n'aurai toujours pas
le bon CV.          
Je postule pour [Le Wagon](https://www.lewagon.com/fr). Ils t'apprennent à coder en gardant cette
vision produit que j'affectionne particulièrement donc c'est parfait !

> Le wagon est un bootcamp en 9 semaines qui survole de manière intensive les 
bases de programmation avant de partir sur un framework web, 
[Ruby on Rails](https://fr.wikipedia.org/wiki/Ruby_on_Rails),  qui 
permet d'avoir une bonne vision 
[fullstack](https://fr.wikipedia.org/wiki/D%C3%A9veloppeur_full_stack) d'un projet.

Mon batch ne commence qu'en octobre, donc je vais utiliser cette periode pour
continuer à approfondir ma stack technique, histoire de ne pas être le guignol
de la classe !

La formation se passe très bien, c'est très intense comme prévu. On apprend beaucoup
de choses, on se voit réaliser des choses formidables. Des petits programmes de
gestion de stock, des démineurs... *Hey mais le backend c'est bien chouette ?!*
Puis on passe au web, on crée des marketplaces, on apprend à manipuler une base
de données, des API, etc. *Wahou, mais le backend côté web, c'est bien coolos aussi ?!*
Durant ces 9 semaines, je ne pense plus qu'au code. Dans ma tête c'est la même
sensation de liberté que lorsque j'ai eu mon permis. Je peux faire ce que je veux.
Sky is the limit.    
Ouais, enfin mes connaissances surtout.

Je rencontre des product managers qui sont venus se perfectionner côte technique,
et plus ils me parlent du métier, moins j'ai l'impression que ce collera
avec ma personalité. Merde, je suis là pour ça moi. Le développement ça me plait,
mais je suis loin d'être le plus rapide lors des excercices; je ne comprends pas tout,
voir completement perdu sur certains sujets qui parraissent limpides pour d'autres.
Donc super réaction d'autodéfense de mon cerveau évolué, *"Nique sa maman, on verra
bien une fois que la formation sera finie, pour l'instant kiff"*.

Super, ça, ça me plait.

### Janvier 2018 - Le jour d'après

A la sortie du wagon, je suis tombé amoureux du développement. Je décide de 
focus sur ça, je veux devenir développeur.     
Durant les 3 premiers mois qui ont suivis, je reviens sur les excercices qui
étaient bloquant pour moi. Je vais plus loin sur des concepts fondamentaux qui
sont nécessaires dans la besace d'un développeur web comme les tests ou les bases
algorithmiques. Je vais plus loin en Javascript aussi.
En gros, je regarde un peu ce qui va de paire avec Ruby on Rails dans les annonces
de jobs, et je vais dans ce sens là.

Je passe en parrallèle des entretiens techniques qui m'aident à m'orienter sur 
mes faiblesses.

S'en suit donc de longs mois, où je vais de refus en refus.                   
*"Arf, mais tu n'as pas fait d'école d'ingé ?"*         
*"Tu n'as pas assez d'experience, désolé"*       
*"Tu ne corresponds pas aux profils que l'on recherche"*       

La période n'est pas simple, le fil du chômage ne dure pas éternellement et tu
penses simplement que tu es une merde. Que t'as quitté un job qui te faisais chier
mais pas tant que ça peut-être au final...

### Juin 2018 - Outscale

Puis parfois la [routourne](https://www.youtube.com/watch?v=CGTPObvxl3U).   
Une boite cherche un junior en Python, voir même quelqu'un à former de 0.                         
Dans cette boite il y a le frère d'un ami. Le bon tuyau.              
Je suis en entretient assez rapidement, et bimbadaboom.            
Je pose mes fesses sur la chaise qui me portera en dehors de la période d'essai.

Mon nouveau *problème* c'est que j'évolue avec des collègues qui sont tous ingénieurs, 
ont des doctorats, même les stagiaires sont beaucoup trop chauds. 
Je suis clairement le seul avec un parcours exotique.                                             
Vous le voyez arriver le fameux *"Moi, je ne suis qu'un imposteur dans tout ça"*.
Venez on parle production musicale, lä je sais plein de choses haha.          

Finalement je finis par me dire que l'idée de base c'est d'apprendre en permanence,
de m'améliorer et je suis dans un environnement parfait pour ça !          
Voilà dans ce sens là, c'est plus sympa de prendre le *problème*.

### Avril 2019 - La suite

Aujourd'hui j'ai toujours l'idée de réaliser un projet dans un coin de ma tête. 
Mais ce n'est plus une priorité. Un jour pourquoi pas.           

Au final je suis passé d'une console à une autre et j'en suis pas peu fier !        

##### **Les échecs c'est cool parfois.**                    
(Et je ne parle pas du jeu !)
