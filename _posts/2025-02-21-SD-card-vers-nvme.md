---
layout: post
title: "Passer le stockage de son Home Assistant d'une carte SD à du NVMe sur un Raspberry Pi 5."
categories: [tutoriel, homelab, homeassistant]
comments: true
---
![Photo RPI-tour-photo]({{ site.baseurl }}/img/posts/rpi-nvme/photo1.jpeg){:width="50%" .img-left}

Sur le Raspberry qui héberge l'OS Home Assistant, j'ai mis à niveau le RPI avec un stockage SSD NVMe pour m'éviter toutes 
galères futures avec la carte SD qui pourrait crasher 🙂.

<!--more-->


## .01 Materiel

- [Raspberry Pi 5](https://shop.mchobby.be/fr/cartes-meres/2755-raspberry-pi-5-16-go-ram-3232100027558.html?src=raspberrypi)
- [Pimoroni NVMe base](https://shop.pimoroni.com/products/nvme-base?variant=41219587178579)
- [Patriot P300 M.2 Gen 3 2280](https://www.amazon.fr/dp/B082BWY2C2?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1)
- [Adaptateur SSD M.2 USB 3](https://www.amazon.fr/dp/B0DKWN77YW?ref=ppx_yo2ov_dt_b_fed_asin_title)
- Un adaptateur pour carte SD vers USB
- Une seconde carte SD
- Un ordinateur

Pour l'installation du pimoroni je vous laisse avec cette [vidéo](https://youtu.be/odG7FbptgWQ?si=yG1bCgyOdl2JaeYB), dans cet article je vais plutôt passer en revue les étapes coté software pour faire sa migration sereinement.

## .02 Le plan

Pourquoi aurait-on besoin d’autant de choses pour simplement changer de type de stockage ?
Il faut réinstaller l'OS sur le SSD, et il va falloir changer le bootloader du Raspberry tout en gardant notre carte SD d'origine au cas où ça tournerait mal 😮 !

## 03. Préparation Home Assistant

Rendez-vous sur votre interface d'home assistant Paramètres > Sauvegarde > et réalisez une sauvegarde manuelle 

![Photo sauvegarde-home-assistant]({{ site.baseurl }}/img/posts/rpi-nvme/sauvegarde_ha.png)

Vous séléctionnez ce que vous avez besoin, mais dans ce que nous souhaitons réaliser, ici il vaut mieux tout cocher.  
Vérifiez que pour vos modules complémentaires ce soit bien sur "Tous". 
Je n'ai pas précisé, mais cette étape de la migration est dans le cas où nous ne voulons pas repartir d'un OS Home Assistant vierge.

Selon ce que vous avez, cela prendra plus ou moins de temps.  

Puis vous téléchargez la sauvegarde.  

![Photo sauvegarde-home-assistant-download]({{ site.baseurl }}/img/posts/rpi-nvme/sauvegarde_ha_download.png)

⚠️ Attention, vous aurez besoin de votre clé de chiffrement pour restaurer la sauvegarde, gardez là bien au chaud quelque part.

## 04. Préparation du SSD

Si vous ne l'avez pas déjà, télécharger le petit [Raspberry Pi Imager](https://www.raspberrypi.com/software/).  
Connectez votre SSD à votre ordi via l'adaptateur et installez Home Assistant dessus via le Pi Imager.  
Home Assistant OS se trouve dans "Other specific-prupose OS > Home assistants and home automation > Home Assistant > Home Assistant OS x.x (RPI 5)"  

## 05. Changer le bootloader du Raspberry
On reste dans le Raspberry Pi Imager, sauf qu'on va jouer avec la carte SD (qui n'est pas votre carte SD qui a déjà Home Assistant).  
Connectez votre carte SD à votre ordi et cette fois selectionnez "Misc utility images > Bootloader (PI 5 Family) > NVMe/USB Boot".  

Cela devrait être rapide à charger sur la nouvelle carte SD. Une fois que c'est fait, vous installez cette nouvelle carte dans le Raspberry et vous démarrez la bête.
Dans l'idée, vous attendez une minute ou deux et vous pouvez à nouveau enlever la carte SD.

## 06. Installation du NVMe

Comme dit plus haut, si vous avez également la base pimoroni cette [vidéo](https://youtu.be/odG7FbptgWQ?si=yG1bCgyOdl2JaeYB) est pas mal.
Puis une fois le nouveau disque SSD installé, hop on relance le raspberry sans carte SD.  

Votre toute nouvelle machine avec un home assistant tout neuf devrait se lancer sans souci.  

![Photo RPI-tour-photo-2]({{ site.baseurl }}/img/posts/rpi-nvme/photo2.jpeg){:width="50%" .img-left}


## 07. Restauration de vos données Home Assistant
Rendez-vous sur homeassistant.local:8123 puis choisissez de restaurer à partir d'une sauvegarde.  
Vous choisissez la sauvegarde que nous avons réalisé plus haut, vous fournissez la clé de chiffrement et il n'y a plus qu'à attendre.

Personnellement l'interface est resté bloqué lors de ma restauration. Après une demie-heure j'ai fini par ouvrir un autre onglet home assistant,
et tout était bien déjà restauré.

![Photo RPI-tour-photo-3]({{ site.baseurl }}/img/posts/rpi-nvme/photo3.jpeg){:width="50%" .img-left}

Enjoy \o
