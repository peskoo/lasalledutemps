---
layout: post
title: "Comment créer un package python ?"
categories: [tutoriel]
comments: true
---

Bien le bonjour, quasiement un an tout pile depuis le dernier article,

Lorssqu'on écrit un programme sympathique, c'est tout de suite plus classe lorsqu'on
peut l'utiliser en évitant une commande à rallonge `python path/to/app.py`.  
Alors revenons ensemble sur un basique, la configuration de notre propre package python.

<!--more--> 

![Photo artistants]({{ site.baseurl }}/img/posts/ARTISANS-BTP.jpeg){:width="50%" .img-left}

On commence par les fondations, l'arborescence du projet:  
```
LaSalleDuTemps/
    setup.py
    lasalledutemps/
        __init__.py
        app.py
```

Là on ne va rien publier sur PyPi, donc on ne va pas s'embêter avec des `README.txt` etc.  
Restons simple.

Une fois que vous aurez reproduis l'arborescence du dessus on aura créé notre package
 **lasalledutemps** grâce au `__init__.py`. Rapide, non ?

Remplissons le `setup.py`:  
```python
from distutils.core import setup

setup(
    description="Un tutoriel pas comme les autres..",
    author="pesko",
    author_email="vegeta@lasalledutemps.fr",
    name="LaSalleDuTemps",
    packages=["lasalledutemps"],
    url="https://lasalledutemps.fr",
    version="0.1.0",
)
```

On rajoute le package fraichement créé à notre liste `packages`.  
Allons donner à manger à ce programme en ouvrant notre `app.py`.  
La ligne directrice de ce tuto c'est la simplicité alors restons là dessus :  
```python
def main():
    print("En live tous les mardi et jeudi à 18h sur twitch.tv/peskoooo")
```

Maintenant configurons le point d'entré, en revenant dans `setup.py` pour ajouter les 
lignes suivantes:    
```python
...
entry_points={
    "console_scripts":
        ["lasalledutemps = lassaledutemps.app:main"],
}
...
```

Tout est bueno. Il ne reste plus qu'à `build` le tout !  
```bash
$ python setup.py sdist
```

Vous venez de créer votre fichier de distribution, il ne manque plus qu'une étape pour 
utiliser votre tout nouveau package. C'est de l'installer via `pip install`.  
```bash
$ pip install path/to/dist/LaSalleDuTemps-0.1.0.tar.gz
```

Et profitez de ce package/programme qui vous sera fort utile au quotidien !

![Photo lasalledutemps-screen]({{ site.baseurl }}/img/posts/screen-package-lasalledutemps.png){:width="50%" .img-left}
