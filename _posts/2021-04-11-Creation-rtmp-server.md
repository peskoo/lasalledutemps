---
layout: post
title: "Créer son serveur RTMP facilement sur Ubuntu."
categories: [tutoriel]
comments: true
---

En ce moment je m'intéresse un peu à tout ce qu'il y a autour du streaming dans la vie réelle.  
Autrement dit, aux streaming IRL.  

Et j'ai régulièrement vu passé comme solution technique d'avoir un serveur RTMP.  
Ok mais pourquoi ? Est-ce que c'est vraiment ce que je cherche ?

<!--more--> 

![Photo client-server-rtmp]({{ site.baseurl }}/img/posts/schema-rtmp-serveur.jpg){:width="50%" .img-left}

Dans un premier temps, [RTMP](https://fr.wikipedia.org/wiki/Real_Time_Messaging_Protocol)
est un protocole, comme son nom l'indique.  
Ce protocole (appartenant à Adobe) va nous permettre de faire voyager des données en
temps réel entre un client (votre téléphone, un drone, une gopro ou que sais-je) et un
serveur (ce que nous allons faire ensemble aujourd'hui).

Ensuite ce serveur communiquera avec OBS pour récupérer le flux vidéos et l'envoyer 
sur twitch.

### Alors pourquoi ?

Si vous streamez depuis votre PC et que d'un coup vous avez besoin d'utiliser une caméra 
pour aller dans d'autre pièces ou en extérieur sans avoir à couper votre stream et simplement
changer de caméra.  
Ou alors si vous voulez utiliser ce serveur pour gérer du multicams aussi par exemple, mais attention 
il y a beaucoup de latence.  
Il y a surement d'autres cas d'usages, mais ils ne me viennent pas là au moment où j'écris cet article.

### Commencons !

Outils:
- Ubuntu 20.04
- NGINX 1.18.0
- Nginx-rtmp-module v1.2.1
- Nginx-init-ubuntu v3.9.0
- OBS studio
- Streamlabs application (téléphone)

Dans mon cas je vais utiliser une machine virtuelle Ubuntu 20.04.  
Il nous faudra recompilier nginx avec le module nginx-rtmp-module.

On va s'update la machine dans un premier temps
```bash
sudo apt update
sudo apt upgrade -y
```
Pensez à ouvrir les ports nécessaires:
- nginx : 80 et 443
- rtmp : 1935 

Il y aura besoin de quelques librairies pour la compilation etc
```bash
sudo apt install wget tar libpcre3-dev zlib1g-dev build-essential libssl-dev libpcre3
```
Dans le répertoire de votre choix, allez chercher nginx et le module
```bash
cd /tmp
wget https://nginx.org/download/nginx-1.18.0.tar.gz
wget https://github.com/arut/nginx-rtmp-module/archive/refs/tags/v1.2.1.tar.gz
tar -zxvf nginx-1.18.0.tar.gz
tar -zxvf v1.2.1.tar.gz
```
On va faire une modif rapide dans le code du module car j'ai eu plusieurs fois une erreur
lors de la compilation qui n'a pas été mergé dans le projet.
```bash
vim nginx-rtmp-module-1.2.1/ngx_rtmp_eval.c
```
Ensuite allez à la ligne 170 (commande vim: 170G ; pour vous rendre directement à la ligne 170)
et rajoutez `/*fall through */`  
```c
case NORMAL:
    switch (c) {
	case '$':
		name.data = p + 1;
		state = NAME;
		continue;
	case '\\':
		state = ESCAPE;
		continue;
    }
    /* fall through */

case ESCAPE:
    ngx_rtmp_eval_append(&b, &c, 1, log);
    state = NORMAL;
    break;
```
Sauvegarder et quitter vim avec `:wq`.

Reprenons notre compilation de nginx :
```bash
cd nginx-1.18.0
./configure --add-module=../nginx-rtmp-module-1.2.1    # certain tuto ajoute l'argument --with-http_ssl_module mais il n'est plus nécessaire avec les version nginx > 1.14
make
sudo make install
```
Sachant qu'on a compiler notre beau nginx il va falloir faire une petite manip pour la
gestion du démon.
```bash
sudo wget https://raw.github.com/JasonGiedymin/nginx-init-ubuntu/master/nginx -O /etc/init.d/nginx
sudo chmod +x /etc/init.d/nginx
sudo update-rc.d nginx defaults    # si vous souhaitez démarrer nginx automatiquement au démarrage du serveur.
```
Maintenant allons faire des petites choses dans notre dossier nginx :
```bash
cd /usr/local/nginx/conf
sudo cp nginx.conf nginx.conf_old    # juste histoire d'avoir un fichier en backup si on fout tout en l'air.
sudo vim nginx.conf
```
A la toute fin du fichier, la ligne en dessous du dernier crochet `}` vous ajoutez:
```
rtmp {
    server {
	listen 1935;
	chunk_size 4096;

	application live {
		live on;
		record off;
	}
    }
}
```
Pour info, sur la ligne `application live`, *live* correspond au nom de l'application qui
sera présent dans l'url finale.  
Enregistrez et quittez puis nous allons tester si la conf est ok avec:
```bash
sudo /usr/local/nginx/sbin/nginx -t
```
Vous devriez voir les lignes
```bash
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
```
Et maintenant on démarre le bousin avec
```
sudo systemctl start nginx
sudo systemctl status nginx    # juste pour check si tout est pas actif, vous devriez voir du vert et du running à gogo !
```
Notre serveur nginx rtmp est en ligne et prêt à être utilisé.  
Vous pouvez tester de vous rendre sur l'url de votre machine http://ip-de-la-machine et
voir une jolie page d'accueil Nginx.

### Maintenant on va du côté du téléphone
Pour cet article j'utilise l'application streamlabs.  
Pour ajouter un serveur RTMP il faut aller dans `Account settings` > cliquer sur *SETUP* à la ligne `Custom RTMP`.

URL : rtmp://ip-de-ta-machine/live  
Stream key: 123  <- ici mettez ce que vous souhaitez. Pour l'exemple 123 c'est parfait.  

Ensuite, lancez le live en choisissant bien le serveur RTMP `Custom RTMP`. Pour qu'OBS puisse récupérer le flux
vidéo vous en avez besoin.  
Juste au cas où, pas d'inquiétude, ca n'intérrompera pas votre live en cours ou autre.

### On passe sur OBS studio
Ajoutez un source `Source média`, pensez bien à décocher `Fichier local` et remplissez
`Entrée` avec votre belle url rtmp://ip-de-ta-machine/live/1234

![Photo obs-studio-source]({{ site.baseurl }}/img/posts/obs-studio-source.png){:width="50%" .img-left}

![Photo obs-studio-rtmp]({{ site.baseurl }}/img/posts/obs-studio-rtmp.png){:width="50%" .img-left}

Tout est bueno, vous devriez voir le flux de votre téléphone apparaitre !

Pour aller plus loin et optimiser le flux vous avez de la configuration disponible [par ici](https://github.com/arut/nginx-rtmp-module/wiki/Directives).

Si vous avez des questions, des soucis, n'hésitez pas à passer poser vos questions sur le
twitter [@Pesko_](https://twitter.com/Pesko_)
