---
layout: post
title: "Comment compiler vim ?"
categories: [tutoriel]
comments: true
---

La version de vim par défaut est sympathique, mais ca l'est tout autant avec une configuration aux petits oignons !  
On est en 2020, est en 2020 python 2 est mort. Repose en paix partenaire !  
Cependant, par défaut, vim n'est pas configuré avec python 3. Alors compilons le avec si vous le voulez bien.  

<!--more-->

### Dans un premier temps

Tu peux installer les paquets nécessaires en fonction des tes envies. Et si as déjà vim sur ta machine c'est tout pareil
en fonction de tes envies; garder les deux ou désinstaller l'ancienne version.   

**Debian**
```bash
$ apt install build-essential libncurses5-dev python3-dev
```

**CentOS**
```bash
$ yum install ncurses-devel python3-devel
```

**curses** c'est parce qu'on est pas à l'abris de faire un petit programme en **C**, globalement vous ajoutez le(s)
langage(s) que vous utilisez, que ce soit du **libperl-dev**, **ruby-dev** ou que sais-je.  

### Téléchargeons la dernière version

```bash
$ git clone https://github.com/vim/vim.git
$ cd vim
$ git pull
```

### Configuration

```bash
./configure --enable-python3interp=yes  --with-python3-config-dir=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu --enable-cscope=yes --with-features=huge --enable-multibyte=yes --enable-gui=no --enable-fail-if-missing
```
Dans l'ordre:  
`--enable-python3interp` active la gestion de python 3 par vim   
`--with-python3-config-dir` est le chemin vers la conf de votre version de python   

  Par exemple ici avec python 3.7:   
    Conf **Debian**: /usr/lib/python3.7/config-3.7m-x86_64-linux-gnu  
    Conf **CentOS**: /usr/lib64/python3.7/config-3.7m-x86_64-linux-gnu  

`--enable-cscope` active la gestion du C  
`--with-features` plusieurs variables sont possibles, voir [ici](http://www.drchip.org/astronaut/vim/vimfeat.html).  
`--enable-multibyte` on en a besoin pour éditer des fichiers de plus de 8 bits. Pour de l'unicode et 
notre bon vieux utf-8 par exemple.  
`--enable-gui` par défaut vim est ok pour fonctionner avec Qt et GTK+, si vous utilisez autre chose il faudra le préciser.  
`--enable-fail-if-missing` permet d'arreter la configuration si une erreur est soulevée.  

Si tu test de reconfigurer, tu peux vider ton cache avec un petit:
```bash
$ make distclean
```

### Compilation

```bash
$ cd src
$ (sudo) make
$ sudo make install
```

Il se peut que tu aies besoin d'utiliser **sudo** pour la commande **make**

### Check

C'est le moment de vérifier si tout s'est vraiment bien passé. 

```bash
$ vim --version
ou
$ vim    # Puis dans vim, :version
```

### Localiser

Si tu avais déjà un vim avant tout ce remue-ménage, je te laisse quelque commandes pour que tu puisses gérer l'ancienne et la nouvelle version.  

- Chopper le chemin de ton ancien vim
```bash
$ whereis vim
```
- Créer un lien symbolique
```bash
$ ln -s source_file target_file
```

- Créer un alias dans ton .bashrc / .zshrc ou autre
```bash
$ echo 'export vim2=chemin/vers/ton/src/vim' >> ~/.bashrc
$ source ~/.bashrc
$ vim2
```

### Problème

Il se peut que vim t'embete avec **syntax.vim**:  
```bash
$ echo 'export VIMRUNTIME=~/chemin/vers/vim/runtime' >> ~/.bashrc
``` 


Si vous rencontrez des soucis, c'est avec plaisir que j'essayerais de vous aider ici ou sur [twitter](https://twitter.com/Pesko_).
